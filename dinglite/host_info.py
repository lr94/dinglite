import os
import platform
import psutil
import distro
import cpuinfo
import gpustat
import pkg_resources


def get_host_info():
    cpu = cpuinfo.get_cpu_info()
    pkgs = list(map(lambda p: "{} {}".format(p.project_name, p.version), pkg_resources.working_set))
    pkgs.sort()

    try:
        gpu = gpustat.new_query()
    except:
        gpu = None

    info = {
        'hostname': platform.node(),
        'os': {
            'family': os.name,
            'os': platform.system().lower(),
            'id': distro.id(),
            'version': distro.version(),
            'kernel': platform.release(),
            'nvidia-driver': gpu.driver_version if gpu is not None else None
        },
        'hw': {
            'cpu': {
                'arch': cpu['arch_string_raw'],
                'count': cpu['count'],
                'brand': cpu['brand_raw'],
                'hz_actual': cpu['hz_actual_friendly']
            },
            'gpus': [{'name': g.name, 'memory_total': g.memory_total * (1024 ** 2), 'uuid': g.uuid} for g in gpu.gpus]
            if gpu is not None else [],
            'memory_total': psutil.virtual_memory().total
        },

        'env': {
            'python_impl': platform.python_implementation(),
            'python_version': platform.python_version(),
            'packages': pkgs
        }
    }

    return info
