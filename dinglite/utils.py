import numpy as np
import hashlib
import time
import platform
import random


def guid(txt: str = None, extra: str = ''):
    if txt is None:
        txt = f'{time.time_ns()}@{platform.node()}:{random.randint(0, 2 ** 128)}/{extra}'
    data = hashlib.md5(txt.encode('utf-8')).digest()
    num = int.from_bytes(data, byteorder='little', signed=False) % (36 ** 24)
    return np.base_repr(num, base=36)
