import sys
import os
import pwd
import time
import warnings
import json
from tqdm import tqdm
from tensorboardX import SummaryWriter
# from torch.utils.tensorboard import SummaryWriter
from .host_info import get_host_info
from .git import *
from .utils import *
from .checkpoint import CheckpointSaver


class Logger:

    def __init__(self, logdir: str = 'tensorboard', run_name=None, use_tb: bool = False, use_tqdm: bool = False,
                 enable_info=True, save_interval: int = 5):
        self.use_tb = use_tb
        self.use_tqdm = use_tqdm
        self.epoch = 0
        self.save_interval = save_interval

        self.last_save = 0
        self.checkpoint = None
        self.resuming = False

        self.log_path = logdir if run_name is None else os.path.join(logdir, run_name)
        if use_tb:
            self.summary_writer = SummaryWriter(log_dir=self.log_path)
        if enable_info:
            os.makedirs(self.log_path, exist_ok=True)

        self.enable_info = enable_info
        if enable_info:
            self.host_info = get_host_info()
            self.run_info = {
                'git': {
                    'repository': get_git_remote_url(),
                    'descriptor': get_git_descriptor()
                },
                'process': {
                    'command': sys.argv,
                    'script_path': os.path.dirname(os.path.realpath(sys.argv[0])),
                    'cwd': os.getcwd(),
                    'pid': os.getpid(),
                    'user': {
                        'id': os.getuid(),
                        'name': pwd.getpwuid(os.getuid()).pw_name
                    }
                },
                'start_time': time.time(),
                'run_name': run_name,
                'logdir': logdir,
                'run_uuid': guid(run_name),
            }
            self.status = {}

            if 'SLURM_JOB_ID' in os.environ:
                self.run_info['scheduler'] = {
                    'name': 'slurm',
                    'env': {k: os.environ[k] for k in filter(lambda k: k.startswith('SLURM'), os.environ.keys())}
                }
            elif 'PBS_JOBNAME' in os.environ:
                self.run_info['scheduler'] = {
                    'name': 'pbs',
                    'env': {k: os.environ[k] for k in filter(lambda k: k.startswith('PBS_'), os.environ.keys())}
                }
            self.save()

    def enable_checkpoint(self, modules, optimizers, schedulers=(), grad_scalers=()):
        self.checkpoint = CheckpointSaver(os.path.join(self.log_path, 'ding_checkpoint.pth'), modules, optimizers,
                                          schedulers, grad_scalers)

    def resume(self):
        if self.checkpoint is None:
            warnings.warn("Checkpoint not enabled, cannot resume")
            return False

        if not self.checkpoint.exists():
            # warnings.warn("No checkpoint to resume")
            return False

        self.epoch, _ = self.checkpoint.load_checkpoint()
        self.resuming = True
        return True

    def progress(self, *args, **kwargs):
        if self.use_tqdm:
            return tqdm(*args, **kwargs)
        else:
            # Alternative output if tqdm is not enabled
            class ProgressIndicator:
                def __init__(self, logger: Logger, total: int, desc: str = "", *args, **kwargs):
                    self.total = total
                    self.desc = desc
                    self.counter = 0
                    self.old_line = ''

                    self.logger = logger

                def __enter__(self):
                    print("Starting {}".format(self.desc))
                    return self

                def __exit__(self, exc_type, exc_val, exc_tb):
                    print("Done {}".format(self.desc))

                def update(self, n: int):
                    self.counter += n
                    line = "{} ({} %)".format(self.desc,
                                              int(self.counter / self.total * 100))
                    if line != self.old_line:
                        print(line)
                        self.old_line = line

                    self.logger.status['current_epoch'] = {
                        'counter': self.counter,
                        'total': self.total
                    }
                    self.logger.save()

            return ProgressIndicator(logger=self, *args, **kwargs)

    def save(self):
        now = time.time()
        if now - self.last_save < self.save_interval:
            return
        self.last_save = now

        if self.enable_info:
            with open(os.path.join(self.log_path, 'dinglite.json'), 'w') as fp:
                self.status['update_time'] = time.time()
                data = {
                    'host_info': self.host_info,
                    'run_info': self.run_info,
                    'status': self.status
                }
                json.dump(data, fp, indent=4, sort_keys=True)

    def add_scalar(self, *args, **kwargs):
        if self.use_tb:
            self.summary_writer.add_scalar(*args, **kwargs)

    def add_images(self, *args, **kwargs):
        if self.use_tb:
            self.summary_writer.add_images(*args, **kwargs)

    def add_image(self, *args, **kwargs):
        if self.use_tb:
            self.summary_writer.add_image(*args, **kwargs)

    def epoch_range(self, *r) -> range:
        r = range(*r)
        if self.resuming:
            r = range(self.epoch + r.step, r.stop, r.step)
            self.resuming = False

        for i in r:
            if i != r.start and self.checkpoint is not None:
                self.checkpoint.save_checkpoint(i - r.step)
                print("Saved checkpoint")

            print("Epoch {} / {}".format(i, r.stop - r.step))
            self.status['epoch'] = i
            self.status['max_epoch'] = r.stop - r.step
            self.save()
            self.epoch = i
            yield i
