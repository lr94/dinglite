import os
import subprocess
import sys


def get_git_descriptor():
    script_dir = os.path.dirname(os.path.realpath(sys.argv[0]))

    try:
        label = subprocess.check_output(['git', 'describe', '--always', '--dirty'],
                                        stderr=subprocess.DEVNULL, cwd=script_dir).decode('utf-8').strip()
    except subprocess.CalledProcessError:
        label = None

    return label


def get_git_remote_url(remote='origin'):
    script_dir = os.path.dirname(os.path.realpath(sys.argv[0]))

    try:
        url = subprocess.check_output(['git', 'config', '--get', 'remote.{}.url'.format(remote)],
                                      stderr=subprocess.DEVNULL, cwd=script_dir).decode('utf-8').strip()
    except subprocess.CalledProcessError:
        url = None

    return url
