import warnings

import torch
from torch.optim.optimizer import Optimizer
from torch.optim.lr_scheduler import _LRScheduler
from torch.cuda.amp.grad_scaler import GradScaler
from torch.nn import Module
import os


class CheckpointSaver:

    def __init__(self, path, modules, optimizers, schedulers=(), grad_scalers=()):
        self.modules = [modules] if isinstance(modules, Module) else modules
        self.optimizers = [optimizers] if isinstance(optimizers, Optimizer) else optimizers
        self.schedulers = [schedulers] if isinstance(schedulers, _LRScheduler) else schedulers
        self.grad_scalers = [grad_scalers] if isinstance(grad_scalers, GradScaler) else grad_scalers
        self.path = path

    def save_checkpoint(self, epoch, scalars=None):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            sched_dict = [s.state_dict() for s in self.schedulers]

        data = {
            'epoch': epoch,
            'scalars': scalars,
            'modules': [m.state_dict() for m in self.modules],
            'optimizers': [o.state_dict() for o in self.optimizers],
            'schedulers': sched_dict,
            'grad_scalers': [gs.state_dict() for gs in self.grad_scalers]
        }

        torch.save(data, self.path)

    def load_checkpoint(self):
        data = torch.load(self.path, map_location=next(self.modules[0].parameters()).device)

        for i, m in enumerate(self.modules):
            m.load_state_dict(data['modules'][i])
        for i, o in enumerate(self.optimizers):
            o.load_state_dict(data['optimizers'][i])

        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            for i, s in enumerate(self.schedulers):
                s.load_state_dict(data['schedulers'][i])

        for i, gs in enumerate(self.grad_scalers):
            gs.load_state_dict(data['grad_scalers'][i])

        return data['epoch'], data['scalars']

    def exists(self):
        return os.path.exists(self.path)

    def delete(self):
        os.unlink(self.path)
