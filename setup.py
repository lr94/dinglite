import setuptools

setuptools.setup(
    name='dinglite',
    version='0.1.11',
    description="Logging utility for ML",
    author="Luca Robbiano",
    author_email="luca.robbiano@iit.it",
    url='https://gitlab.com/lr94/dinglite',
    license='MIT',
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
    install_requires=['tqdm', 'tensorboardX', 'distro', 'psutil', 'py-cpuinfo', 'gpustat', 'numpy']
)

